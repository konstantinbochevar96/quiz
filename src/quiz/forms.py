from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, modelformset_factory
from django import forms

from quiz.models import Choice


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('Quantity of questions is out of range ({}..{})'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        order = []

        for form in self.forms:
            order.append(form.cleaned_data['order_number'])

            if 1 not in order:
                raise ValidationError('Order number must start with 1')

            if order.count(form.cleaned_data['order_number']) > 1:
                raise ValidationError(f'Number {form.cleaned_data["order_number"]} is already used')

        for i in range(len(sorted(order)) - 1):
            if order[i + 1] != i + 2:
                raise ValidationError('Numbers must go in ascending order in one step')


class ChoiceInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('Quantity of answers is out of range ({}..{})'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        total_number = sum(
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        )

        if total_number == len(self.forms):
            raise ValidationError('NOT allowed to select all choices')

        if total_number == 0:
            raise ValidationError('At LEAST 1 choice should be selected')


class ChoiceForm(ModelForm):
    is_selected = forms.BooleanField(required=False)

    class Meta:
        model = Choice
        fields = ['text']


ChoiceFormSet = modelformset_factory(
    model=Choice,
    form=ChoiceForm,
    extra=0
)
