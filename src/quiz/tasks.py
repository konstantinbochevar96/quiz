import datetime

from celery import shared_task

from quiz.models import Result


@shared_task
def cleanup_outdated_results():
    outdated_tests = Result.objects.filter(
        state=Result.STATE.NEW,
        write_date__lte=datetime.datetime.now() - datetime.timedelta(seconds=7 * 24 * 3600)
    )
    outdated_tests.delete()

    print('Outdated results deleted!')
