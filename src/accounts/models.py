from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    email_sent = models.DateTimeField(null=True, auto_now=True)
    motivation_count = models.IntegerField(default=0, blank=True)

    def save(self, *args, **kwargs):
        self.rating = min(100, self.rating)
        self.full_clean()
        super().save(*args, **kwargs)
