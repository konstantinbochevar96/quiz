from PIL import Image # noqa

from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, Form, fields

from accounts.models import User

from django import forms


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'bio', 'birth_date']

    # def save(self, commit=True):
    #     super().save(commit)
    #     image = Image.open(self.instance.image)
    #     image.thumbnail((300, 300), Image.ANTIALIAS)
    #     image.save(self.instance.image.path)


class ContactUs(Form):
    subject = fields.CharField(max_length=256, empty_value='Message from Quiz')
    message = fields.CharField(widget=forms.Textarea)
