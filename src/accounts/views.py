from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.core.mail import send_mail
from django.shortcuts import render # noqa
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView
from django.views.generic.edit import UpdateView, FormView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm, ContactUs
from accounts.models import User
# Create your views here.


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully registered')
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} successfully logged in')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.info(self.request, f'User {self.request.user} has been logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm
    paginate_by = 5

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        return context

    def get_queryset(self):
        return self.object.results.all().select_related('test')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'Profile successfully updated')
        return result


class UserPasswordChangeView(PasswordChangeView):
    model = User
    success_url = reverse_lazy('accounts:profile')
    template_name = 'password-change.html'


class ContactUsView(LoginRequiredMixin, FormView):
    template_name = 'contact_us.html'
    success_url = reverse_lazy('index')
    form_class = ContactUs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            send_mail(
                subject=form.cleaned_data['subject'],
                message=form.cleaned_data['message'],
                from_email=request.user.email,
                recipient_list=settings.EMAIL_HOST_RECIPIENT.split(':'),
                fail_silently=False,
            )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
