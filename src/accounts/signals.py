import datetime

from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import User
from accounts.tasks import check_test


@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    from accounts import roles
    if created:
        instance.groups.add(roles.USERS)


@receiver(post_save, sender=User)
def new_user(sender, instance, created, **kwargs):
    if created:
        check_test.apply_async(args=(instance.id,), eta=datetime.datetime.now() + datetime.timedelta(hours=24))
