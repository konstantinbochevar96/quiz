import datetime

from celery import shared_task

from django.core.mail import send_mail
from django.utils import timezone

from accounts.models import User

from quiz.models import Result


@shared_task
def check_test(id): # noqa
    result = Result.objects.filter(user_id=id)
    user = User.objects.get(id=id)
    if result.count() == 0:
        send_mail(
            subject='Welcome to Quiz!',
            message="Hi, it's time to pass at least one test on our website. Enjoy!",
            from_email='admin@admin.com',
            recipient_list=[user.email],
            fail_silently=False,
        )
    print('Email sent!')


@shared_task
def sent_motivation_letter():
    results = Result.objects.filter(
        state=Result.STATE.NEW,
        write_date__lte=datetime.datetime.now() - datetime.timedelta(seconds=5 * 24 * 3600)
    )
    for result in results:
        user = User.objects.get(id=result.user.id)
        quarter = user.email_sent + datetime.timedelta(days=90)
        if user.motivation_count < 2 and timezone.now() > quarter:
            user.motivation_count += 1
            user.email_sent = datetime.datetime.now()
            user.save()
            send_mail(
                subject="Quiz:Unfinished test",
                message='Hi! You have unfinished test on Quiz, please continue him on our website',
                from_email='admin@admin.com',
                recipient_list=[result.user.email],
                fail_silently=False,
            )

    print('Email sent!')
