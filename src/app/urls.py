"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


API_PREFIX = 'api/v1'


urlpatterns = [

    path('admin/', admin.site.urls),

    path('', TemplateView.as_view(template_name='index.html'), name='index'),

    path('accounts/', include('accounts.urls')),

    path('quizes/', include('quiz.urls')),

    path('__debug__/', include(debug_toolbar.urls)),

    path(f'{API_PREFIX}/', include('quiz.api.urls')),
    path(f'{API_PREFIX}/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(f'{API_PREFIX}/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]

urlpatterns += \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler400 = 'quiz.views.error_400'
handler403 = 'quiz.views.error_403'
handler404 = 'quiz.views.error_404'
handler500 = 'quiz.views.error_500'
